//
//  AssociateSwiftApp.swift
//  AssociateSwift
//
//  Created by Jonas Wallmann on 20.06.22.
//

import SwiftUI

@main
struct AssociateSwiftApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
